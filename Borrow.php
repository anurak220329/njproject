<html>
<?php 
include 'header.php';
include "config.php";
$tech_list = $tech_obj->tech_list();
?>

<head>
<link href="https://unpkg.com/bootstrap-table@1.18.2/dist/bootstrap-table.min.css" rel="stylesheet">

<script src="https://unpkg.com/tableexport.jquery.plugin/tableExport.min.js"></script>
<script src="https://unpkg.com/bootstrap-table@1.18.2/dist/bootstrap-table.min.js"></script>
<script src="https://unpkg.com/bootstrap-table@1.18.2/dist/bootstrap-table-locale-all.min.js"></script>
<script src="https://unpkg.com/bootstrap-table@1.18.2/dist/extensions/export/bootstrap-table-export.min.js"></script>

<style>
  .select,
  #locale {
    width: 100%;
  }
  .like {
    margin-right: 10px;
  }
</style>
</head>
<body>
<br></br>   
        <!-- Search filter -->
		<?php

ini_set('display_errors', 1);
error_reporting(~0);

$strKeyword = null;

if(isset($_POST["txtKeyword"]))
{
	$strKeyword = $_POST["txtKeyword"];
}
?>
<?php

ini_set('display_errors', 1);
error_reporting(~0);

$strKeywordno = null;

if(isset($_POST["txtKeywordno"]))
{
$strKeywordno = $_POST["txtKeywordno"];
}
?>
<?php
require("connect.php"); //connect
// SELECT * FROM customer WHERE CustomerID ="Cus002" or 1=1; //or 1=1 แสดงทั้งหมดที่เป็นเงื่อนไขเป็นจริง คือ SQL injection
$sql = "SELECT
orders.order_idcard, orders.order_id, orders.order_date, orders.order_name, land.file_id, land.land_id, orders_items.quantity, orders_items.lands_id, land.no, orders_items.order_id, land.box_id
FROM orders
JOIN orders_items
ON orders_items.order_id = orders.order_id
JOIN land
ON land.id = orders_items.lands_id WHERE land_id LIKE '%".$strKeyword."%'
";
$stmt = $conn->prepare($sql);
$stmt->execute();

?>

<div class="row">
<div class="container">
<form name="frmSearch" method="post" action="<?php echo $_SERVER['SCRIPT_NAME'];?>">
<center>
<label style="font-size : 24px; color: #000000;">ตรวจค้นผู้ยืมจากเลขที่ดิน</label>
</center>
  <center>
<div class="col-sm-3">
            <div class="input-group">
         
            
              <input type="text" name="txtKeyword" style="font-size : 24px; color: #000000;"  class="form-control" placeholder="เลขที่ดิน" value="<?php echo $strKeyword;?>">
              
              <div class="input-group-append">
                <span class="input-group-text"><i class="nc-icon nc-zoom-split"></i></span>
                <input type="submit" value="ค้นหา">
              </div>
            </div>
            
          </div>
          </center>
</form>
</div>
<br></br>
</div>
<a  href="borrowlate.php" class="btn btn-danger btn-xs btn-md" style="font-size : 24px; color: #000000; font-family: 'Kanit', sans-serif; ">รายชื่อนายช่างที่ยืมต้นร่างเกินกำหนด</a>
<br></br>
<div class="select">
  <select  class="form-control" id="locale">
    <option value="af-ZA">af-ZA</option>
    <option value="ar-SA">ar-SA</option>
    <option value="ca-ES">ca-ES</option>
    <option value="cs-CZ">cs-CZ</option>
    <option value="da-DK">da-DK</option>
    <option value="de-DE">de-DE</option>
    <option value="el-GR">el-GR</option>
    <option value="en-US" >en-US</option>
    <option value="es-AR">es-AR</option>
    <option value="es-CL">es-CL</option>
    <option value="es-CR">es-CR</option>
    <option value="es-ES">es-ES</option>
    <option value="es-MX">es-MX</option>
    <option value="es-NI">es-NI</option>
    <option value="es-SP">es-SP</option>
    <option value="et-EE">et-EE</option>
    <option value="eu-EU">eu-EU</option>
    <option value="fa-IR">fa-IR</option>
    <option value="fi-FI">fi-FI</option>
    <option value="fr-BE">fr-BE</option>
    <option value="fr-FR">fr-FR</option>
    <option value="he-IL">he-IL</option>
    <option value="hr-HR">hr-HR</option>
    <option value="hu-HU">hu-HU</option>
    <option value="id-ID">id-ID</option>
    <option value="it-IT">it-IT</option>
    <option value="ja-JP">ja-JP</option>
    <option value="ka-GE">ka-GE</option>
    <option value="ko-KR">ko-KR</option>
    <option value="ms-MY">ms-MY</option>
    <option value="nb-NO">nb-NO</option>
    <option value="nl-NL">nl-NL</option>
    <option value="pl-PL">pl-PL</option>
    <option value="pt-BR">pt-BR</option>
    <option value="pt-PT">pt-PT</option>
    <option value="ro-RO">ro-RO</option>
    <option value="ru-RU">ru-RU</option>
    <option value="sk-SK">sk-SK</option>
    <option value="sv-SE">sv-SE</option>
    <option value="th-TH" selected>th-TH</option>
    <option value="tr-TR">tr-TR</option>
    <option value="uk-UA">uk-UA</option>
    <option value="ur-PK">ur-PK</option>
    <option value="uz-Latn-UZ">uz-Latn-UZ</option>
    <option value="vi-VN">vi-VN</option>
    <option value="zh-CN">zh-CN</option>
    <option value="zh-TW">zh-TW</option>
  </select>
</div>

<table
  id="table"
  data-toolbar="#toolbar"
  
 
  data-show-toggle="true"
  data-show-fullscreen="true"
  data-show-columns="true"
  data-show-columns-toggle-all="true"
 
  data-show-export="true"
  data-click-to-select="true"
  data-detail-formatter="detailFormatter"
  data-minimum-count-columns="2"
  
  
  data-id-field="id"
  data-page-list="[10, 25, 50, 100, all]"
  data-show-footer="true"
  data-side-pagination="server"
 
  data-response-handler="responseHandler">

 
  <thead>
    <tr>
    
    <th>ชื่อผู้ยืม</th>
    <th>เลขระวาง</th>
    <th>เลขที่ดิน</th>
    <th>เลขที่เล่ม</th>
    <th>เลขกล่อง</th>
    <th>วันที่ยืม</th>
    <th>จำนวนวันที่ยืม</th>
    <th></th>
    <th class="text-right"><button class="btn btn-info btn-xs" onclick="window.print()">พิมพ์ข้อมูล</button></th>
    </tr>
  </thead>
  <tbody>
  <?php
while($result = $stmt->fetch ( PDO::FETCH_ASSOC)) 
{
  $datestart = $result["order_date"];
  $dateend = date("Y-m-d");
 
$calculate =strtotime("$dateend")-strtotime("$datestart");
$summary=floor($calculate / 86400); 

?>
   <tr>
   
    <td><?php echo $result["order_name"] ?></td>
    <td><?php echo $result["file_id"] ?></td>
    <td><?php echo $result["land_id"] ?></td>
    <td><?php echo $result["no"] ?></td>
    <td><?php echo $result["box_id"] ?></td>
    <td><?php echo $result["order_date"] ?></td>
    <td><b style="<?=($summary >= 15 ? 'color:red' : 'black')?>; font-size : 24px;  font-family: 'Kanit', sans-serif; "><?php echo $summary ?></b></td>
    <td align="center"><a class='btn btn-success btn-xs' href="updatenext.php?id=<?php echo $result["order_id"];?>" onClick="javascript:return confirm('คุณต้องการจะยืมต้นร่างต่อใช่หรือไม่');">ยืมต้นร่างต่อ</td>
    <td align="center"><a class='btn btn-danger btn-xs' href="deletes.php?lands_id=<?php echo $result["lands_id"];?>" onClick="javascript:return confirm('คุณต้องการจะคืนต้นร่างใช่หรือไม่');">คืนต้นร่าง</td>
    
    </tr>
    <?php
}
?>
  </tbody>
</table>

</body>
<script>

  var $table = $('#table')
  var $remove = $('#remove')
  var selections = []

  function getIdSelections() {
    return $.map($table.bootstrapTable('getSelections'), function (row) {
      return row.id
    })
  }

  function responseHandler(res) {
    $.each(res.rows, function (i, row) {
      row.state = $.inArray(row.id, selections) !== -1
    })
    return res
  }

  function detailFormatter(index, row) {
    var html = []
    $.each(row, function (key, value) {
      html.push('<p><b>' + key + ':</b> ' + value + '</p>')
    })
    return html.join('')
  }

  function operateFormatter(value, row, index) {
    return [
      '<a class="like" href="javascript:void(0)" title="Like">',
      '<i class="fa fa-heart"></i>',
      '</a>  ',
      '<a class="remove" href="javascript:void(0)" title="Remove">',
      '<i class="fa fa-trash"></i>',
      '</a>'
    ].join('')
  }

  window.operateEvents = {
    'click .like': function (e, value, row, index) {
      alert('You click like action, row: ' + JSON.stringify(row))
    },
    'click .remove': function (e, value, row, index) {
      $table.bootstrapTable('remove', {
        field: 'id',
        values: [row.id]
      })
    }
  }

  function totalTextFormatter(data) {
    return 'Total'
  }

  function totalNameFormatter(data) {
    return data.length
  }

  function totalPriceFormatter(data) {
    var field = this.field
    return '$' + data.map(function (row) {
      return +row[field].substring(1)
    }).reduce(function (sum, i) {
      return sum + i
    }, 0)
  }

  function initTable() {
    $table.bootstrapTable('destroy').bootstrapTable({
      height: 550,
      locale: $('#locale').val(),
     
    })
    $table.on('check.bs.table uncheck.bs.table ' +
      'check-all.bs.table uncheck-all.bs.table',
    function () {
      $remove.prop('disabled', !$table.bootstrapTable('getSelections').length)

      // save your data, here just save the current page
      selections = getIdSelections()
      // push or splice the selections if you want to save all data selections
    })
    $table.on('all.bs.table', function (e, name, args) {
      console.log(name, args)
    })
    $remove.click(function () {
      var ids = getIdSelections()
      $table.bootstrapTable('remove', {
        field: 'id',
        values: ids
      })
      $remove.prop('disabled', true)
    })
  }

  $(function() {
    initTable()

    $('#locale').change(initTable)
  })
</script>
</html>