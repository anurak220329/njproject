<?php
class Products {
  // "Link" to parent core
  private $core = null;
  function __construct ($core) {
    $this->core = $core;
  }

  function getAll () {
  // getAll () : get all products

    return $this->core->fetch(
      "SELECT * FROM `land`", 
      null, "id"
    );
  }

  function get ($id) {
  // getAll () : get specified product

    return $this->core->fetch(
      "SELECT * FROM `land` WHERE `box_id`=?", 
      [$id], -1
    );
  }
}