<html>
<?php
include 'header.php';
$tech_list = $tech_obj->tech_list();
?>
<body>
<div class="container " > 
<br></br>
    <div class="row content">
    
    
        <h3>รายชื่อบุคลากร</h3>
        <?php
        if (isset($_SESSION['message'])) {
            echo "<p class='custom-alerts'>" . $_SESSION['message'] . "</p>";
            unset($_SESSION['message']);
        }
        ?>
       

       <table class="table table-striped custab">
  <thead>
    <tr>
    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ชื่อ-นามสกุล</th>
    
    <th class="text-right"></th>
    </tr>
  </thead>
  <tbody>
  <?php
if ($tech_list->num_rows > 0) {
  while ($row = $tech_list->fetch_assoc()) {
     ?>
    <tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row["member_name_lastname"] ?></td>
   
    <td class="text-right">
                    <a  href="<?php echo 'delete.php?id=' . $row["member_id"] ?>" class='btn btn-danger btn-xs' onClick="javascript:return confirm('คุณต้องการจะลบรายชื่อใช่หรือไม่');">ลบ</a>  
                    <a  href="<?php echo 'update.php?id=' . $row["member_id"] ?>" class='btn btn-info btn-xs'>แก้ไข</a>  
                    <a  href="<?php echo 'searchdate.php?id=' . $row["member_id"] ?>" class='btn btn-primary'>ดูประวัติการยืม</a>  
                </td>
    </tr>
    <?php
    }
}
?>
  </tbody>
</table>




    </div>
</div>
<?php
include 'footer.php';
?>  
</body>

</html>