<!DOCTYPE html>
<html lang="en" >
    <head>
  
      </style>
        <title>nj system</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">  
        <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@200&display=swap" rel="stylesheet">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="./assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="./assets/css/paper-kit.css?v=2.2.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="./assets/demo/demo.css" rel="stylesheet" /><link rel="stylesheet" href="public/theme.css">
    </head>
    <body >
        <?php
       
        include './class/tech.php';
        $tech_obj = new tech();
        error_reporting(0);
        ?>  
<!-- Navigation -->
<nav class="navbar navbar-expand-lg bg-success">
            <div class="container">
              <a class="navbar-brand" style="font-size : 24px;  color: #000000; font-family: 'Kanit', sans-serif;  " href="index.php">หน้าหลัก</a> 
              <a class="navbar-brand" style="font-size : 24px; color: #000000; font-family: 'Kanit', sans-serif; "  href="add.php">เพิ่มบุคลากร</a>
              
              <a class="navbar-brand" style="font-size : 24px; color: #000000; font-family: 'Kanit', sans-serif; " href="search.php">ยืมต้นร่าง</a>
              <a class="navbar-brand" style="font-size : 24px; color: #000000; font-family: 'Kanit', sans-serif; " href="Borrow.php">คืนต้นร่าง</a>
             
            
                  <div class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" style="font-size : 24px; color: #000000; font-weight:bold; font-family: 'Kanit', sans-serif; " id="dropdownMenuButton" href="#pk" role="button" aria-haspopup="true" aria-expanded="false">จัดการต้นร่าง</a>
                    <ul class="dropdown-menu dropdown-info" aria-labelledby="dropdownMenuButton">
                      
                      <a class="dropdown-item" href="addland.php">เพิ่มต้นร่าง</a>
                      <a class="dropdown-item" href="editland.php">แก้ไขต้นร่าง</a>
                      <a class="dropdown-item" href="deleteland.php">ลบต้นร่าง</a>
                      
                    </ul>
                  </div>
              <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar-success" aria-controls="navbarNav" aria-expanded="true" aria-label="Toggle navigation">
                <span class="navbar-toggler-bar" style="font-size : 24px; color: #000000; font-family: 'Kanit', sans-serif; ">เพิ่มบุคลากร</span>
                <span class="navbar-toggler-bar"></span>
                <span class="navbar-toggler-bar"></span>
              </button>
              <div class="navbar-collapse collapse show" id="navbar-success" style="">
                <ul class="navbar-nav ml-auto">
                  
                  <li class="nav-item">
                    <a class="navbar-brand" style="font-size : 24px; color: #000000; font-family: 'Kanit', sans-serif; " href=""><?=$_SESSION['member_name_lastname']?></a>
                  </li>
                  </li>
                  <li class="nav-item">
                    <a class="navbar-brand" style="font-size : 24px; color: #000000; font-family: 'Kanit', sans-serif; " href="logout.php">ออกจากระบบ</a>
                  </li>
                </ul>
              </div>
            </div>
          </nav>

<!-- Full Page Image Header with Vertically Centered Content -->

  
<div class="col-sm-3 col-md-2 mr-auto ml-auto">
</br>
              <img src="./image/ตรากรมที่ดิน.png" class="img-circle img-no-padding img-responsive" alt="Rounded Image">
            </div>
      <div class="col-12 text-center">
        <h1 class="font-weight-light">สำนักงานที่ดินกรุงเทพมหานคร สาขาหนองจอก</h1>
        
      </div>
    </div>
  </div>
  <script src="https://kit.fontawesome.com/c0829f4ffc.js" crossorigin="anonymous"></script>
 <!--   Core JS Files   -->
 <script src="./assets/js/core/jquery.min.js" type="text/javascript"></script>
    <script src="./assets/js/core/popper.min.js" type="text/javascript"></script>
    <script src="./assets/js/core/bootstrap.min.js" type="text/javascript"></script>
  
  
   
  
    


</body>
</html>