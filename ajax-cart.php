<?php
/* (A) INIT */
require __DIR__ . DIRECTORY_SEPARATOR . "lib" . DIRECTORY_SEPARATOR . "core.php";
$_CC->extend("Cart");

switch ($_POST['req']) {
  /* (B) INVALID REQUEST */
  default:
    echo "INVALID REQUEST";
    break;

  /* (C) ADD ITEM TO CART */
  case "add":
    $pass = $_CC->Cart->add($_POST['product_id'], 1);
    echo json_encode([
      "status" => $pass,
      "message" => $pass ? "add complete" : $_CC->error
    ]);
    break;

  /* (D) CHANGE QTY */
  case "change":
    $pass = $_CC->Cart->change($_POST['product_id'], $_POST['qty']);
    echo json_encode([
      "status" => $pass,
      "message" => $pass ? "Quantity updated" : $_CC->error
    ]);
    break;

  /* (E) COUNT TOTAL NUMBER OF ITEMS */
  case "count":
    echo json_encode([
      "status" => 1,
      "count" => $_CC->Cart->count()
    ]);
    break;

  /* (F) SHOW CART */
  case "show":
    $products = $_CC->Cart->getAll();
    $sub = 0;
    $total = 0; ?>
    <h1>ต้นร่างที่ยืม</h1>
    <table id="scTable">
      <tr>
        <th>นำออก</th>
        <th>จำนวน</th>
        <th>เลขระวาง</th>
        <th>เลขที่ดิน</th>
        <th>เลขที่เล่ม</th>
        <th>เลขที่กล่อง</th>
        
      </tr>
      <?php
      if (count($_SESSION['cart'])>0) {
      foreach ($_SESSION['cart'] as $id => $qty) {
        $sub = $qty * $products[$id]['product_price'];
        $total += $sub; ?>
      <tr>
        <td>
          <input class="scDel bRed" type="button" value="X" onclick="cart.remove(<?= $id ?>);"/>
        </td>
        <td><input id='qty_<?= $id ?>' onchange='cart.change(<?= $id ?>);' type='number' value='<?= $qty ?>'/></td>
        <td><?= $products[$id]['file_id'] ?></td>
        <td><?= $products[$id]['land_id'] ?></td>
        <td><?= $products[$id]['no'] ?></td>
        <td><?= $products[$id]['box_id'] ?></td>
        
      </tr>
      <?php }} else { ?>
      <tr><td colspan="3">ไม่มีต้นร่างที่เลือก</td></tr>
      <?php } ?>
      <tr>
        <td colspan="2"></td>
       
      </tr>
    </table>
    <?php if (count($_SESSION['cart']) > 0) { ?>
    <form id="scCheckout" onsubmit="return cart.checkout();">
      <h1>ผู้ยืมต้นร่าง</h1>
      
      <div class="form-group">
    <label for="exampleFormControlSelect1" style="font-size : 18px; color: #000000;">ชื่อนายช่างรังวัด</label>
    <select class="form-control" style="font-size : 17px; color: #000000;" id="co_name">
    <option value="นายธวัชชัย มนมิตร">นายธวัชชัย มนมิตร</option>
  <option value="นายชิษณุพงศ์ ศะศิวณิชย์">นายชิษณุพงศ์ ศะศิวณิชย์</option>
  <option value="นางเมตต์พิตตา ฤทธิศิลป์">นางเมตต์พิตตา ฤทธิศิลป์</option>
  <option value="นายสมพงษ์ ศรีหะทัย">นายสมพงษ์ ศรีหะทัย</option>
  <option value="นายภูษิญ มณีโชติ">นายภูษิญ มณีโชติ</option>
  <option value="นายนัทธี อิ่มสอาด">นายนัทธี อิ่มสอาด</option>
  <option value="นายคฑาหัส สมหวัง">นายคฑาหัส สมหวัง</option>
  <option value="นายศิวาวุธ กลิ่นหอม">นายศิวาวุธ กลิ่นหอม</option>
  
    </select>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1" style="font-size : 18px; color: #000000;">เลขประจำตัวประชาชน</label>
    <input type="text" class="form-control" id="co_email" aria-describedby="emailHelp" style="font-size : 17px; color: #000000;" placeholder="กรอกเลขประจำตัวประชาชน13หลัก">
    
  </div>
     
      <input type="submit" class="bRed" value="ทำการยืม"/>
    </form>
    <?php }
    break;

  /* (G) CHECKOUT */
  case "checkout":
    // NOTE: You might want to do more security checks + payment here
    $pass = $_CC->Cart->checkout($_POST['name'], $_POST['email']);
    echo json_encode([
      "status" => $pass,
      "message" => $pass ? "Order confirmed" : $_CC->error
    ]);
    break;
  
 
  case "checkout-email":
    $pass = $_CC->Cart->checkout($_POST['name'], $_POST['email']);
    if ($pass) {
      $pass= $_CC->Cart->emailOrder($_CC->orderID);
    }
    echo json_encode([
      "status" => $pass,
      "message" => $pass ? "Order confirmed" : $_CC->error
    ]);
    break;
}