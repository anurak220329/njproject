<?php
class Tech
{
    private $conn;
    function __construct() {
    session_start();
    $servername = "localhost";
    $dbname = "nj_system";
    $username = "root";
    $password = "";
  

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    mysqli_set_charset($conn, "utf8");
    // Check connection
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
       }else{
        $this->conn=$conn;  
       }

    }


    public function tech_list(){
        
       $sql = "SELECT * FROM member ORDER BY member_id asc ";
       $result=  $this->conn->query($sql);
       return $result;  
    }
    
    public function borrow_list(){
        
      $sql = "SELECT * FROM orders ORDER BY member_id desc ";
      $result=  $this->conn->query($sql);
      return $result;  
   }


    public function add($post_data=array()){
         
       if(isset($post_data['create'])){
       $member_name_lastname= mysqli_real_escape_string($this->conn,trim($post_data['member_name_lastname']));
       $member_level= mysqli_real_escape_string($this->conn,trim($post_data['member_level']));
      

       $sql="INSERT INTO member (member_name_lastname, member_level) VALUES ('$member_name_lastname', '$member_level')";
        
        $result=  $this->conn->query($sql);
       
           if($result){
           
               $_SESSION['message']="เพิ่มข้อมูลสำเร็จ";
               
              header('Location: index.php');
           }
          
       unset($post_data['create']);
       }
       
        
    }

    public function addland($post_data=array()){
         
      if(isset($post_data['adds'])){
      $land_id= mysqli_real_escape_string($this->conn,trim($post_data['land_id']));
      $file_id= mysqli_real_escape_string($this->conn,trim($post_data['file_id']));
      $box_id= mysqli_real_escape_string($this->conn,trim($post_data['box_id']));
      $no= mysqli_real_escape_string($this->conn,trim($post_data['no']));
      
     

      $sql="INSERT INTO land (land_id, file_id, box_id, no ) VALUES ('$land_id', '$file_id',  '$box_id',  '$no')";
       
       $result=  $this->conn->query($sql);
       
          if($result){
          
              $_SESSION['message']="เพิ่มข้อมูลสำเร็จ";
              
             header('Location: addland.php');
          }
         
      unset($post_data['adds']);
      }
      
       
   }
    
    public function view_tech($id){
       if(isset($id)){
       $member_id= mysqli_real_escape_string($this->conn,trim($id));
      
       $sql="Select * from member where member_id='$member_id'";
        
       $result=  $this->conn->query($sql);
     
        return $result->fetch_assoc(); 
    
       }  
    }
    
    
    public function update_tech_info($post_data=array()){
       if(isset($post_data['update_tech'])&& isset($post_data['id'])){
           
       $member_name_lastname= mysqli_real_escape_string($this->conn,trim($post_data['member_name_lastname']));
       $member_level= mysqli_real_escape_string($this->conn,trim($post_data['member_level']));
       $member_id= mysqli_real_escape_string($this->conn,trim($post_data['id']));

       $sql="UPDATE member SET member_name_lastname='$member_name_lastname', member_level='$member_level' WHERE member_id =$member_id";
     
        $result=  $this->conn->query($sql);
        
           if($result){
               $_SESSION['message']="Successfully Updated ";
           }
       unset($post_data['update_tech']);
       }   
    }
    
    public function delete($id){
        
       if(isset($id)){
       $member_id= mysqli_real_escape_string($this->conn,trim($id));

       $sql="DELETE FROM  member  WHERE member_id =$member_id";
        $result=  $this->conn->query($sql);
        
           if($result){
               $_SESSION['message']="ลบรายชื่อบุคลากรสำเร็จ";
            
           }
       }
       echo '<script type="text/javascript">'; 
       
       echo 'window.location.href = "index.php";';
       echo '</script>';
    }
    function __destruct() {
    mysqli_close($this->conn);  
    }

    public function deletess($id){
        
      if(isset($id)){
      $member_id= mysqli_real_escape_string($this->conn,trim($id));

      $sql="DELETE FROM  land  WHERE id = $id";
       $result=  $this->conn->query($sql);
       
          if($result){
              $_SESSION['message']="ลบระวางสำเร็จ";
           
          }
      }
        header('Location: deleteland.php'); 
   }


   public function updatenext($id){
        
      if(isset($id)){
      $order_id= mysqli_real_escape_string($this->conn,trim($id));
      $today = date("Y-m-d");
     $sql="UPDATE orders SET order_date='$today' WHERE order_id = $order_id";
       $result=  $this->conn->query($sql);
       
          if($result){
              $_SESSION['message']="ยืมต้นร่างต่อสำเร็จ";
           
          }
      }
      echo '<script type="text/javascript">'; 
       
      echo 'window.location.href = "Borrow.php";';
      echo '</script>';
   }
  
   


    public function deletes($id){
      global $sql;
      if(isset($id)){
       
      $order_id= mysqli_real_escape_string($this->conn,trim($id));
      $land_id= mysqli_real_escape_string($this->conn,trim($id['land_id']));
      $quantity= mysqli_real_escape_string($this->conn,trim($id['quantity']));
      $sql = "INSERT INTO history  (order_id, land_id, quantity) VALUES ('$order_id', '$land_id', '$quantity')";
      $results =  $this->conn->query($sql);
      $sql="DELETE 
      FROM orders_items      
      WHERE
          order_id = $order_id";
      
      
     $result=  $this->conn->query($sql);
          if($result){
              $_SESSION['message']="คืนระวางเรียบร้อย";
        
          }
      }
        header('Location: Borrow.php'); 
   }
  
    
}

  

?>