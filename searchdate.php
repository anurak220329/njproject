
<?php 
include 'header.php';
include "config.php";
$member_id = $_GET['id']
?>

<!doctype html>
<html>
    <head>
        <title>jQuery Datepicker to filter records with PHP MySQL</title>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
       <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
        <!-- CSS -->
        <link href='jquery-ui.min.css' rel='stylesheet' type='text/css'>

       
        </script>
       
    </head>
    <body >
        <br></br>   
        <!-- Search filter -->
        <form method='post' action=''>
        <div  class="row">
        <div class="col-4">
        วันที่<input id="datepicker" autocomplete="off" width="270"  name='fromDate' value='<?php if(isset($_POST['fromDate'])) echo $_POST['fromDate']; ?>' />
        </div>
        <div class="col-4">
        ก่อนถึงวันที่<input id="datepicker1" autocomplete="off" width="270"  name='endDate' value='<?php if(isset($_POST['endDate'])) echo $_POST['endDate']; ?>' />
        
         </div> 
         <input class="btn btn-info btn-xs" style="font-size : 20px; " type='submit' name='but_search' value='ค้นหา'>&nbsp;&nbsp;
         <button class="btn btn-warning btn-xs" style="font-size : 20px; " onclick="window.print()">พิมพ์ข้อมูล</button>

         </div>  

           
        </form>

        <!-- Employees List -->
        <div  >
            </br> 
        <table class="table table-striped custab">
                <tr>
                    <th>ชื่อผู้ยืม</th>
                    <th>วันที่ยืม</th>
                    
                    
                    <th>เลขระวาง</th>
                    <th>เลขที่ดิน</th>
                     <th>เลขที่เล่ม</th>
                    <th>เลขกล่อง</th>
                    
                </tr>

                <?php
                $emp_query = "SELECT
                orders.order_idcard, member.member_id, orders.order_name, orders.order_date, land.file_id, land.land_id, land.no, history.order_id, orders.order_id, land.box_id
               FROM orders
               JOIN history
                 ON history.order_id = orders.order_id
               JOIN land
                 ON land.id = history.lands_id 
              
                 JOIN member
                 ON member.member_name_lastname = orders.order_name
                  WHERE orders.order_id = history.order_id && member.member_id = '$member_id' ";

                // Date filter
                if(isset($_POST['but_search'])){
                    $fromDate = $_POST['fromDate'];
                    $endDate = $_POST['endDate'];

                    if(!empty($fromDate) && !empty($endDate)){
                        $emp_query .= " and order_date between '".$fromDate."' and '".$endDate."' ";
                    }
                }

                // Sort
                $emp_query .= " ORDER BY order_date DESC";
                $employeesRecords = mysqli_query($con,$emp_query);

                // Check records found or not
                if(mysqli_num_rows($employeesRecords) > 0){
                    while($empRecord = mysqli_fetch_assoc($employeesRecords)){
                        $order_id = $empRecord['order_id'];
                        $order_name = $empRecord['order_name'];
                        $order_date = $empRecord['order_date'];
                       
                        $file_id = $empRecord['file_id'];
                        $land_id = $empRecord['land_id'];
                        $no = $empRecord['no'];
                        $box_id = $empRecord['box_id'];
                        echo "<tr>";
                        echo "<td>". $order_name ."</td>";
                        echo "<td>". $order_date ."</td>";
                        
                        echo "<td>". $file_id ."</td>";
                        echo "<td>". $land_id ."</td>";
                        echo "<td>". $no ."</td>";
                        echo "<td>". $box_id ."</td>";
                       
                        echo "</tr>";
                    }
                }else{
                    echo "<tr>";
                    echo "<td colspan='4'>ไม่พบการค้นหา</td>";
                    echo "</tr>";
                }
                ?>
            </table>
            
        <!-- MDB -->
        <script>
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap',
            format: 'yyyy-mm-dd'
        });
    </script>
    <script>
        $('#datepicker1').datepicker({
            uiLibrary: 'bootstrap',
            format: 'yyyy-mm-dd'
        });
    </script>
 
    </body>
</html>
