<?php
include 'header.php';
if (isset($_GET['id'])) {
    $tech_info = $tech_obj->view_tech($_GET['id']);
    if (isset($_POST['update_tech']) && $_GET['id'] === $_POST['id']) {
        $tech_obj->update_tech_info($_POST);
    }
} else {
    header('Location: index.php');
}
?>
<div class="container " > 
    <div class="row content">
        
        <h3>แก้ไขรายชื่อบุคลากร</h3>
        <?php
        if (isset($_SESSION['message'])) {
            echo "<p class='custom-alert'>" . $_SESSION['message'] . "</p>";
            unset($_SESSION['message']);
        }
        ?>

        <hr/>
        <div class="container">
        <form method="post" action="">
            <input type="hidden" name="id" value="<?php if (isset($tech_info['member_id'])) {
            echo $tech_info['member_id'];
        } ?>" id=""  >
            <div class="form-group">
                <label for="member_name_lastname">ชื่อ:</label>
                <input type="text" name="member_name_lastname" value="<?php if (isset($tech_info['member_name_lastname'])) {
                   echo $tech_info['member_name_lastname'];
        } ?>" id="member_name_lastname" class="form-control" required maxlength="50">
            </div>
    
            <label for="member_level">ระดับ:</label>
            <div  class="form-group" >
            <select class="form-control" id="member_level" style="font-size : 17px; color: #000000;" name="member_level" >
            <option id="member_level" name="member_level"  value="<?php if (isset($tech_info['member_level'])) {
            echo $tech_info['member_level'];
        } ?>"><?php
            echo $tech_info['member_level']; ?>
        </option>
            <option id="member_level" name="member_level"  value="นายช่างรังวัดอาวุโส">นายช่างรังวัดอาวุโส</option>  
  <option id="member_level" name="member_level" value="นายช่างรังวัดชำนาญงาน">นายช่างรังวัดชำนาญงาน</option>
  <option id="member_level" name="member_level" value="นายช่างรังวัดปฎิบัติงาน">นายช่างรังวัดปฎิบัติงาน</option>
  <option id="member_level" name="member_level" value="ตำแหน่งอื่นๆ">ตำแหน่งอื่นๆ</option>
  
</select>
          </div>

            <input type="submit" class="btn btn-success" name="update_tech" value="บันทึก"/>
            <a href="index.php"><button type="button" class="btn btn-warning ">ย้อนกลับ </button></a> 
            
            
        </form> 
        </div>
    </div>
</div>
<hr/>
<?php
include 'footer.php';
?>

